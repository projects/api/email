var noticeTimer, noticeShow = false;

function closeNotice() {
	if(noticeShow === true) {
		clearTimeout(noticeTimer);
		emailNotice.animate({ marginTop: '-=51px' }, 300);
		noticeShow = false;
	}
}

function showNotice(type, message) {
	if(!document.getElementById('svcCheckEmail_notice')) {
		var notice = '<div id="svcCheckEmail_notice"><div class="notice_center_auto">';
			notice += '<div id="notice_text"></div>';
			notice += '<div id="notice_close_btn" onclick="closeNotice();">&nbsp;</div>';
			notice += '</div></div>';
		
		emailNotice = $(notice);
		$('body').prepend(emailNotice);
	}
	
	$('#notice_text').html(message);
	
	emailNotice.removeClass().addClass(type);
	$('#notice_close_btn').removeClass().addClass(type);
	
	if(noticeShow === false) {
		emailNotice.animate({ marginTop: '+=51px' }, 300);
		noticeShow = true;
	}
	
	clearTimeout(noticeTimer);
	noticeTimer = setTimeout(function() {
		emailNotice.animate({ marginTop: '-=51px' }, 300);
		noticeShow = false;
	}, 3000);
}

$(function() {
	$('<link />').attr({
		rel: 'stylesheet',
		type: 'text/css',
		href: 'http://eterfund.ru/api/email/svcCheckEmail.css'
	}).appendTo('head');
	
	var prev_emailCheck, checkDoneStatus = 'fail';
	var emailInput = $('#svcCheckEmail');
	var emailNotice, emailForm = emailInput.closest('form');
	
	emailForm.submit(function() {
		if(emailInput.val().trim() == '')
			return true;
		
		if(checkDoneStatus != 'done') {
			if(checkDoneStatus == 'fail') {
				showNotice('red', 'Введённый вами адрес недоступен. Пожалуйста, укажите корректный email.');
			}
			
			return false;
		}
		
		closeNotice();
		
		return true;
	});
	
	emailInput.blur(function() {
		var email = emailInput.val().trim();
		var regex = /^([^@]+)@(([a-zA-Z0-9\_\-]+\.)+([a-zA-Z]{2}|aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|post|pro|tel|travel|xxx))$/;
		
		emailInput.val(email);
		
		if(prev_emailCheck == email) {
			return false;
		}
		
		prev_emailCheck = email;
		checkDoneStatus = 'fail';
		
		if(email.length == 0) {
			if(noticeShow === true) {
				clearTimeout(noticeTimer);
				emailNotice.css({ marginTop: '-51px' });
				noticeShow = false;
			}
			
			emailInput.removeClass('bg-loader').removeClass('bg-true').removeClass('bg-false');
			
			return false;
		}
		
		inputHeight = parseInt(emailInput.height()) + parseInt(emailInput.css('margin-top')) + parseInt(emailInput.css('margin-bottom'));
		
		showNotice('yellow', 'Идет проверка email адреса, это может занять несколько секунд.');
		
		checkDoneStatus = 'check';
		emailInput.removeClass('bg-true').removeClass('bg-false').addClass('bg-loader');
		
		clearTimeout(noticeTimer);
		
		if(email.length < 5 || !regex.test(email)) {
			showNotice('red', 'Неверный формат email. Адрес должен иметь вид: <b>имя_пользователя@имя_домена</b> (например <b>somebody@example.com</b>)');
			emailInput.removeClass('bg-loader').addClass('bg-false');
			checkDoneStatus = 'fail';
			
			return false;
		}
		
		$.ajax({
			url: 'http://eterfund.ru/api/email/svcCheckEmail.php',
			type: 'GET',
			data: {
				email: email
			},
			error: function() {
				showNotice('red', 'Ошибка при проверке адреса. Пожалуйста, повторите попытку позже.');
				emailInput.removeClass('bg-loader').addClass('bg-false');
				
				checkDoneStatus = 'fail';
			},
			success: function(response) {
				response = JSON.parse(response);
				
				if(response.status === false) {
					showNotice('red', 'Введенный вами адрес недоступен. Пожалуйста, укажите корректный email.');
					emailInput.removeClass('bg-loader').addClass('bg-false');
					
					checkDoneStatus = 'fail';
				}
				else {
					showNotice('green', 'Email адрес указан верно.');
					emailInput.removeClass('bg-loader').addClass('bg-true');
					
					checkDoneStatus = 'done';
				}
			}
		});
		
		return true;
	});
});