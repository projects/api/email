<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type');

error_reporting(0);

$email = $_GET['email'];

if(mb_strlen($email, 'utf8') > 5 && 
	preg_match('/^([^@]+)@(([a-zA-Z0-9\_\-]+\.)+([a-zA-Z]{2}|aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|post|pro|tel|travel|xxx))$/', $email, $matches)) {
	
	$response = array(
		'email' => $matches[0],
		'username' => $matches[1],
		'domain' => $matches[2],
		'status' => true,
		'error' => null
	);
	
	if(checkdnsrr($matches[2]) === false) {
		$response['status'] = false;
		$response['error'] = 'dns_records_not_found';
	}
	
	$referer = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : 'Unknown';
	$referer_host = $_SERVER['HTTP_REFERER'] ? str_ireplace('www.', '', parse_url($referer, PHP_URL_HOST)) : '.unknown_host';
	
	$data = 'Date: '.date('d/m/Y H:i:s').
		' | Referer: '.$referer.
		' | Query e-mail: '.$response['email'].
		' | Username: '.($response['username'] ? $response['username'] : 'null').
		' | Domain: '.($response['domain'] ? $response['domain'] : 'null').
		' | Status: '.($response['status'] ? 'true' : 'false').
		' | Error: '.($response['error'] ? $response['error'] : 'null').
		PHP_EOL;
	
	file_put_contents(dirname(__FILE__).'/logs/'.$referer_host.'.log', $data, FILE_APPEND);
}
else {
	$response = array(
		'email' => $email,
		'username' => null,
		'domain' => null,
		'status' => false,
		'error' => 'wrong_email_format'
	);
}

print json_encode($response);

?>